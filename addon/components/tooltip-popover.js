import Component from '@ember/component';
import $ from 'jquery';

export default Component.extend({
  tagName: '',
  init() {
    this._super(...arguments);
    this.set("popoverIconClass", this.getWithDefault("iconClass", 'fa-info-circle'));
    this.set("popoverIconColor", this.getWithDefault("iconColor", 'black'));
  },
  actions: {
    hidePopover(){
      $('#toggle-popover').click();
    }
  }
});
